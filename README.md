# RESUME

## 이용현

010-3361-2519 | lyoungh2570@gmail.com



## 전문 기술

- 웹 프런트

**운영체제:** Windows, Mac OS X, Android and Linux
**언어:** HTML, CSS, SASS, JavaScript, NodeJS, ReactJS, JQuery, git
**데이터베이스:** MySQL, MongoDB, AWS

## 개인적인 기술

- 프로젝트 경험.
- 높은 책임 의식.
- 동기 부여되고 항상 새로운 것을 배우려함.
- 독립적이고 자립적인 작업 처리
- 공동 작업 및 팀 또는 그룹 작업 가능
- 드라마와 영화를 사랑함.

## 프로젝트

### Albus (진행중):

개인 프로젝트로 웹 브라우저로 쉽게 전자 앨범을 만들 수 있는 서비스 제작.

- Demo: <https://albus-service.ml>
- Stack:
  - backend: Node Js, Express Js, Typescript, Postgresql, AWS EC2
  - frontend: React, Webpack, Sass, AWS S3, AWS cloudfront

### Hexo blog (운영중):

원래는 gitlab에서 jekyll를 이용한 블로그였으나 github에 Hexo를 이용한 블로그를 만들어 이전했다.

공부한 내용을 정리하거나 개발 때 에러 해결한 내용등을 기록하는데 사용한다.

- url : https://yonghyun-lee.github.io/
- Stack: Hexo, github

### DID Project:

DID 패널을 통합 관리하는 서버. 학교에 홍보나 공지, 광고용으로 영상을 반복적으로 재생하는 디스플레이 패널들은 원래 USB로 구동하고 있었다. 수정이나 새 영상을 추가하려면 직접가서 다 바꿔야 하는 문제가 있었고 이 불편함을 줄이기 위해 디스플레이 패널을 통합 관리하는 시스템을 구축했다. 또한 공무원들이 항상 출근 때 디스플레이 전원을 키고 퇴근 시에 끄는 작업을 매일 하고 있는데 그 불편함을 줄이기 위해 HK 스마트 플러그를 이용해 자동 전원 제어 기능을 구현했다.

#### 실제 학교 기관에 납품되어 7개의 학과에서 사용중.

![스크린샷 2019-02-25 오후 5.18.01](/Users/yonghyun/Project/resume/images/스크린샷 2019-02-25 오후 5.18.01.png)

- service-url: [https://did-hbnu.techandslow.com](https://did-hbnu.techandslow.com/)
- server Stack:
  - backend: Node Js, Express Js, mongoDB, AWS, NGINX
  - frontend: JQuery, ejs, bootstrap
- raspberry pi stack: shell script, python3, crontab, pm2

### Work process Project:

작업 프로세스 관리 프로그램

- Repository: <https://gitlab.com/yonghyunlee/work-process-project>
- Stack:
  - backend: Django, mySQL
  - frontend: JQuery, bootstrap

### auto measurement program

회사 요구사항에 따라 C언어로 개발한 계측 장비 자동화 프로그램

- Repository: <https://gitlab.com/yonghyunlee/auto-Measurement>
- Stack: C, NI library

### jekyll blog

공부한 내용을 기록한 블로그

- Homepage: <https://yonghyunlee.gitlab.io/>
- Stack: jekyll

### 배터리 벨트 프로젝트

BLE을 이용해 배터리들을 관리할 수 있는 벨트 개발 연구

- Stack: Android, Java
- log: [https://medium.com/@lyoungh2570/android-ble-%ED%86%B5%EC%8B%A0-%EC%98%88%EC%A0%9C-%EB%B6%84%EC%84%9D-8d01507a1177](https://medium.com/@lyoungh2570/android-ble-통신-예제-분석-8d01507a1177)

## 예비 전문 경험

**뉴리더 양성 프로그램 참여**
실무 경험
(2018 년 6 월 - 2018 년 12 월)

- 외부 회사와 공동으로 회사에서 일하고 계측 장비 자동화 프로젝트 수행
- DID 프로젝트 진행

## 교육

**정보통신공학과 학사** 
한밭대학교
(3월 2016 – 3월 2020)

**관련된 과목들:** C Programming, Advanced C Programming, Object-oriented programming(C++), Java Programming, Mobile programming, Database, Data Structure, Algorithm, Linear algebra, Data communication, Computer network(TCP/IP), UNIX Systems, Operating system, Mobile Programming(Android Programming)